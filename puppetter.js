const puppeteer = require('puppeteer');

const account = "admin@com7erp.onmicrosoft.com";
const password = "COM7@D365";

(async () => {
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();
  await page.goto('https://com7.operations.dynamics.com/?cmp=COM7&mi=DefaultDashboard');
  
    let emailHandle = await page.$('#i0116');
    emailHandle.focus();
    await simulate_key_press(page, account);
    

    let passHandle = await page.$('#i0118');
    passHandle.focus();
    await simulate_key_press(page, password);
    
    LoginButton(page, '#idSIButton9');
    page.waitForSelector('#idSIButton9[value="Sign in"]').then((element) => {element.click()});
    page.waitForSelector('#idSIButton9[value="Yes"]').then((element) => {element.click()});
})();

async function simulate_key_press(element, str, numbers = 50) {
    for (let x in str) {
        await element.keyboard.press(str[x], {delay: numbers});
    }
}

async function LoginButton (page, id) {
   await page.waitForSelector(id).then((element) => {
       element.click();
    });
}